import {Button, StyleSheet, Text, TextInput, View} from 'react-native';
import React from 'react';

interface LoginPageProps {}

const LoginPage: React.FC<LoginPageProps> = ({}) => {
  return (
    <View>
      <View style={{alignItems: 'center'}}>
        <View style={styles.main_container}>
          <View style={styles.top_container}>
            <View style={styles.row_align}>
              <View style={styles.circle}>
                <Text style={styles.circle_text}>D</Text>
              </View>
              <Text style={styles.heading}>DesignApp</Text>
            </View>
          </View>
          <View style={styles.bottom_container}>
            <View style={{paddingVertical: 30}}>
              <Text style={styles.main_title}>TEAM LEAD LOGIN</Text>
              <Text style={styles.sub_title}>Welcome to the appName</Text>
              <Text style={styles.label}>Username</Text>
              <TextInput
                style={styles.input}
                placeholder="Username"
                keyboardType="numeric"
              />

              <View style={{padding: 8}}></View>
              <Text style={styles.label}>Password</Text>
              <TextInput
                style={styles.input}
                placeholder="Password"
                keyboardType="numeric"
              />
              <Text
                style={[
                  styles.label,
                  {alignSelf: 'flex-end', paddingRight: 10},
                ]}>
                Forget Password ?
              </Text>
              <View style={{padding: 15}}></View>
              <View style={{marginBottom: 50, backgroundColor: '#0047C8'}}>
                <View style={styles.button}>
                  <Button color={'#1A44C6'}  title="Login"></Button>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default LoginPage;

const styles = StyleSheet.create({
  main_container: {
    // width: '22%',
    // padding: 10,
    backgroundColor: '#61A4F8',
    flex: 1,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.5,
    shadowRadius: 7,
    borderRadius: 20,
  },
  top_container: {
    // height: '13%',
    backgroundColor: 'white',
    alignItems: 'center',
    borderStartEndRadius: 20,
    borderStartStartRadius: 20,
  },

  bottom_container: {
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  main_title: {
    marginHorizontal: 60,
    fontSize: 23,
    color: '#B5D1F9',
    // alignItems: 'center',
  },
  sub_title: {
    marginHorizontal: 60,
    // alignItems: 'center',
    paddingLeft: '7%',
    color: '#B5D1F9',
    fontSize: 13,
    paddingBottom: 40,
  },
  input: {
    height: 40,
    width: '100%',

    padding: 10,
    borderRadius: 20,
    backgroundColor: 'white',
    color: '#D8D8D8',
  },
  label: {
    // paddingLeft:10,
    padding: 5,
    color: '#CBE0FC',
  },
  row_align: {flexDirection: 'row', alignItems: 'center', padding: 30},
  circle: {
    height: 30,
    width: 30,
    backgroundColor: '#005AE3',
    borderRadius: 20,
  },
  circle_text: {
    color: 'white',
    alignSelf: 'center',
    paddingTop: 5,
    fontWeight: '600',
  },
  heading: {fontSize: 30, fontWeight: '500', paddingLeft: 10, color: '#676767'},
  button: {
    height: 40,

    justifyContent: 'center',
  },
});
