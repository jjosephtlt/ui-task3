/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';

import {createNativeStackNavigator} from '@react-navigation/native-stack';

import LoginPage from './screens/LoginPage';
const Stack = createNativeStackNavigator();

function App(): React.JSX.Element {
  return (
    <SafeAreaView
      style={{backgroundColor: '#9FB7F1', height: '100%', paddingTop: '10%'}}>
      <LoginPage></LoginPage>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({});

export default App;
